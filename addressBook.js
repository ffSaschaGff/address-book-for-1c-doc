"use strict";
let app = {};
let userData = {};

document.addEventListener("DOMContentLoaded", async function () {
  Vue.component("tree-department-node", {
    props: ["elementDepartment"],
    template: "#treeDepartmentNode",
  });

  Vue.component("tree-node", {
    props: ["elementDepartment"],
    template: "#treeNode",
    methods: {
      onDepartmentClick: function (elementDepartment) {
        elementDepartment.open = !elementDepartment.open;
        setTimeout(checkPhotos, 100);
      },
    },
  });

  app = new Vue({
    el: "#addressBook",
    data: { searchName: "", departments: await getAddressBookTree() },
    methods: {
      filterByName: function () {
        this.departments.forEach(
          this.searchName
            ? this._setNameFilterRecursive
            : this._clearNameFilterRecursive
        );
        this.openAllDepartments(this.departments);
        setTimeout(checkPhotos, 100);
      },
      openAllDepartments: function (departments) {
        departments.forEach((department) => {
          department.open = true;
          app.openAllDepartments(department.departments);
        });
      },
      _setNameFilterRecursive: function (department) {
        department.users.forEach(
          (user) =>
            (user.open =
              user.name.toUpperCase().indexOf(app.searchName.toUpperCase()) !=
              -1)
        );
        department.departments.forEach(app._setNameFilterRecursive);
      },
      _clearNameFilterRecursive: function (department) {
        department.users.forEach((user) => (user.open = true));
        department.departments.forEach(app._setNameFilterRecursive);
      },
    },
  });
  checkPhotos();
});

window.addEventListener("scroll", checkPhotos);

//tree bilder
async function getAddressBookTree() {
  let flatDepartments = await getDepartments();
  let users = await getUsers();
  let result = [];
  fillAddressBookTreeRecursive(
    result,
    users,
    flatDepartments,
    "00000000-0000-0000-0000-000000000000"
  );
  return result;
}

function fillAddressBookTreeRecursive(
  container,
  users,
  flatDepartments,
  parent
) {
  for (let flatDepartmentElement of flatDepartments) {
    if (flatDepartmentElement.parent != parent) continue;

    let departmentOfTree = {};

    //department info
    departmentOfTree.name = flatDepartmentElement.name;
    departmentOfTree.uid = flatDepartmentElement.uid;
    departmentOfTree.open = true;
    departmentOfTree.departments = [];
    fillAddressBookTreeRecursive(
      departmentOfTree.departments,
      users,
      flatDepartments,
      flatDepartmentElement.uid
    );
    //users info
    let usersOfDepartment = users[flatDepartmentElement.uid];
    for (let user of usersOfDepartment) {
      user.open = true;
      user.showPhoto = false;
      userData[user.uid] = user;
    }
    departmentOfTree.users = usersOfDepartment ? usersOfDepartment : [];
    for (let i = 1; i < departmentOfTree.users.length; i++) {
      if (departmentOfTree.users[i].uid == flatDepartmentElement.boss) {
        let boss = departmentOfTree.users.splice(i, 1);
        departmentOfTree.users.unshift(...boss);
        break;
      }
    }

    container.push(departmentOfTree);
  }
}

//get data fron API
async function getDepartments() {
  let response = await fetch("addressBookDepartments.json");
  return await response.json();
}

async function getUsers() {
  let response = await fetch("addressBookUsers.json");
  return await response.json();
}

//check and load photo on scroll
function checkPhotos() {
  document.querySelectorAll("[data-user]").forEach((userElement) => {
    if (elementIsVisible(userElement)) {
      userData[userElement.dataset.user].showPhoto = true;
    }
  });
}

function elementIsVisible(target) {
  let targetPosition = {
    top: window.pageYOffset + target.getBoundingClientRect().top,
    bottom: window.pageYOffset + target.getBoundingClientRect().bottom,
  };
  let windowPosition = {
    top: window.pageYOffset,
    bottom: window.pageYOffset + document.documentElement.clientHeight,
  };

  if (
    targetPosition.bottom > windowPosition.top &&
    targetPosition.top < windowPosition.bottom
  ) {
    return true;
  }
  return false;
}
